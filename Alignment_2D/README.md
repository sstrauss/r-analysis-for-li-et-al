# R analysis for Li et al

This repository contains custom data analysis scripts developed for the paper Li et al. 2023 "Cell cycle-driven growth reprogramming encodes plant age into leaf morphogenesis"

Alignment_2D:
contains raw files (CSV files exported from MorphoGraphX), R-scripts and plots.
The scripts generate 2D alignment plots from raw cellular data. The data is arranged and binned by proximal-distal and medial-lateral position
