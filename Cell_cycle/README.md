# R analysis for Li et al

This repository contains custom data analysis scripts developed for the paper Li et al. 2023 "Cell cycle-driven growth reprogramming encodes plant age into leaf morphogenesis"

Cell_cycle
contains CSV cell lineage tracking files from MorphoGraphX, R-scripts and plots.
The scripts load the cell ineages and compute the duration of cell cycles as the time duration between two division events

