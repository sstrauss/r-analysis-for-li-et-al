library(ggplot2)
library(grDevices)
library(scales)
library(tidyverse)
library(reshape2)
library(gridExtra) # to save the tables
library(plotly)
library(data.tree)

# initialize data frames and lists
s = seq(from = stopT, to = startT, by = -1)
AllFiles <- c()
for(i in 2:length(s)){
  #print(s[i])
  fileString = paste("T",s[i],"T",s[i-1],sep="")
  AllFiles <- c(AllFiles,c(fileString))
}

allTable <- list() # list of data frames, saves all opened files
naList <- list()

# initalize running counter variable, starting from the last time point, counting down
counter = stopT

# now read the single files into a larger structure (allTable) 
# which will save the lineage traced labels of individual cells across multiple time steps
# read the files from the last to the first


# build allTable from the last file forward
for(f in AllFiles){
  InputFile<- paste(SamplePath,"/",f,".csv",sep="")
  print(InputFile)
  currentFile <- read.csv(InputFile,header= T)
  
  pString =paste("label",counter,sep="")
  pStringNext =paste("label",counter-1,sep="")
  prolifString = paste("prolif",counter,sep="")
  
  if(counter == stopT){ # handling of the first file
    allTable[pString] <- list(rep("",nrow(currentFile)))
    allTable[pStringNext] <- list(rep("",length(allTable[[pString]])))
    
    for(i in 1:nrow(currentFile)){
      allTable[[pString]][i] = paste(counter,"_",currentFile$Label[i],sep="")
      allTable[[pStringNext]][i] = paste(counter-1,"_",currentFile$Parent.Label[i],sep="")
    }
    
    # for now fill with 0s
    allTable[prolifString] <- list(rep(0,length(allTable[[pString]])))
    allTable["totalProlif"] <- list(rep(0,length(allTable[[pString]])))

  } else { # handling of all files after having read the first one
    
    # for now fill with 0s
    allTable[pStringNext] <- list(rep("",length(allTable[[pString]])))
    allTable[prolifString] <- list(rep(0,length(allTable[[pString]])))
    
    #for(i in 1:nrow(currentFile)){
      for(j in 1:length(allTable[[pString]])){ # go through all the labels (which are already here, as they were the parents of the previous file)
        plabel <- allTable[[pString]][j]
        splitStringForLabel = str_split(plabel,"_")
        adjustedLabel = splitStringForLabel[[1]][2]
        llabel <- subset(currentFile, Label == adjustedLabel) # extract that label
        if(nrow(llabel) > 1){
          print("sth weird happened, a parent exists multiple times, this shouldn't happen!")
        } else if(nrow(llabel) > 0){
          allTable[[pStringNext]][j] = paste(counter-1,"_",llabel[1,"Parent.Label"],sep="")
        } else { # the label didnt exist in the later time point, so we ignore the parent
          allTable[[pStringNext]][j] = NA
        }
      }
    #}
  }
  # count the number of daughter cells in the current file
  for(j in 1:length(allTable[[pString]])){
    splitStringForLabel = str_split(allTable[[pStringNext]][j],"_")
    adjustedLabel = splitStringForLabel[[1]][2]
    llabel <- subset(currentFile, Parent.Label == as.numeric(adjustedLabel))
    allTable[[prolifString]][j] <- nrow(llabel)
  }
  
  # update the sum of daughter cells
  for(j in 1:length(allTable[[pString]])){
    if(allTable[[prolifString]][j] > 0){
      allTable[["totalProlif"]][j] =  allTable[["totalProlif"]][j] + (allTable[[prolifString]][j]-1)
    } else {
      allTable[["totalProlif"]][j] = -1
    }
    
  }
  
  counter = counter - 1
}
print("Read all Files succesfully!")
pString =paste("label",stopT,sep="")

# compute and extract 

allTable["exactCCD"] <- list(rep(0,length(allTable[[pString]])))
totalTime = hoursBetweenTPs*(stopT - startT)
allTable["divs"] <- list(rep(0,length(allTable[[pString]])))

allTable["div1T"] <- list(rep(0,length(allTable[[pString]])))

#print("Table")
for(j in 1:length(allTable[[pString]])){
  pString =paste("label",startT,sep="")
  if(allTable[["totalProlif"]][j] >= 0 & !is.na(allTable[[pString]][j])){
    allTable[["exactCCD"]][j] = allTable[["totalProlif"]][j]
    if(allTable[["exactCCD"]][j] == 0){
      allTable[["divs"]][j] = 0
      allTable[["exactCCD"]][j] = NA
    } else if(allTable[["exactCCD"]][j] == 1){
      allTable[["divs"]][j] = 1
      allTable[["exactCCD"]][j] = NA
      
      # go through all prolif tables, build vector
      vecProlifs = c()
      for(i in 1:length(s)){
        prolifString = paste("prolif",s[i],sep="")
        vecProlifs = c(vecProlifs,c(allTable[[prolifString]][j]))
      }
      
      # find the distance between the two twos
      firstTwo = -1
      for(k in 2:(length(s))){
        prolifString = paste("prolif",s[k-1],sep="")
        if(allTable[[prolifString]][j] == 2){
          if(firstTwo == -1){
            firstTwo = k-1
          }
        }
      }
      
      cellCycleDur = max(firstTwo, length(s)-firstTwo)
      allTable[["divs"]][j] = 1
      allTable[["div1T"]][j] <- length(s)-firstTwo+startT
      
      
    } else if(allTable[["exactCCD"]][j] == 2){
      # go through all prolif tables, build vector
      vecProlifs = c()
      for(i in 1:length(s)){
        prolifString = paste("prolif",s[i],sep="")
        vecProlifs = c(vecProlifs,c(allTable[[prolifString]][j]))
      }
      
      # check if exactly two division occured!
      if(sum(vecProlifs==2) == 2 & sum(vecProlifs==0) == 0){
        #print(vecProlifs)
        
        # find the distance between the two twos
        firstTwo = -1
        secondTwo = -1
        for(k in 2:length(s)){
          prolifString = paste("prolif",s[k-1],sep="")
          if(allTable[[prolifString]][j] == 2){
            if(firstTwo == -1){
              firstTwo = s[k-1]
            } else {
              secondTwo = s[k-1]
            }
          }
        }
        cellCycleDur = firstTwo - secondTwo
        allTable[["exactCCD"]][j] =  cellCycleDur*hoursBetweenTPs
        allTable[["divs"]][j] = 2
      }
    } else if(allTable[["exactCCD"]][j] == 3){
      # go through all prolif tables, build vector
      vecProlifs = c()
      for(i in 1:length(s)){
        prolifString = paste("prolif",s[i],sep="")
        vecProlifs = c(vecProlifs,c(allTable[[prolifString]][j]))
      }
      
      # check if exactly three divisions occured!
      if(sum(vecProlifs==2) == 3 & sum(vecProlifs==0) == 0){
        #print(vecProlifs)
        
        # find the distance between the two twos
        firstTwo = -1
        secondTwo = -1
        thirdTwo = -1
        for(k in 2:length(s)){
          prolifString = paste("prolif",s[k-1],sep="")
          if(allTable[[prolifString]][j] == 2){
            if(firstTwo == -1){
              firstTwo = s[k-1]
            } else if(secondTwo == -1) {
              secondTwo = s[k-1]
            } else {
              thirdTwo = s[k-1]
            }
          }
        }
        cellCycleDur = ((firstTwo - secondTwo) + (secondTwo - thirdTwo))/2
        #ccList[["ccd"]][j] = cellCycleDur*hoursBetweenTPs
        allTable[["exactCCD"]][j] =  cellCycleDur*hoursBetweenTPs
        allTable[["divs"]][j] = 3
      }
    } else {
      # go through all prolif tables, build vector
      vecProlifs = c()
      for(i in 1:length(s)){
        prolifString = paste("prolif",s[i],sep="")
        vecProlifs = c(vecProlifs,c(allTable[[prolifString]][j]))
      }
      
      # check if exactly two division occured!
      if(sum(vecProlifs==2) > 2 & sum(vecProlifs==0) == 0){
        #print(vecProlifs)
        
        # find the distance between the pairs of twos and average them
        avgDis = 0
        avgDisCount = 0
        
        # find the first two
        k = 1
        posFirst = -1
        firstTwo = -1
        nextTwo = -1
        while (k < length(s)) {
          prolifString = paste("prolif",s[k],sep="")
          if(allTable[[prolifString]][j] == 2){
            firstTwo = s[k]
            posFirst = k
            k = length(s)
          }
          k=k+1
        }
        #print(posFirst)
        
        # continue until next is found
        while(posFirst < length(s)){
          k = posFirst+1
          while(k < length(s)){
            prolifString = paste("prolif",s[k],sep="")
            if(allTable[[prolifString]][j] == 2){
              #print(k)
              nextTwo = s[k]
              #only consider the last div
              if(avgDisCount == 0){
                avgDis = avgDis + s[posFirst]-s[k]
                avgDisCount = avgDisCount + 1
              }
              
              posFirst = k
              k = length(s)
            }
            k=k+1
            if(k >= length(s)){
              posFirst = length(s)
            }
          }
        }
        #print(avgDis)
        #print(avgDisCount)
        allTable[["divs"]][j] = allTable[["exactCCD"]][j]
        allTable[["exactCCD"]][j] =  NA
        
      }
    }
  } else {
    allTable[["exactCCD"]][j] = -1
  }
  
}



for(j in 1:length(allTable[[pString]])){
  # go through all prolif tables, build vector
  vecProlifs = c()
  for(i in 1:length(s)){
    prolifString = paste("prolif",s[i],sep="")
    vecProlifs = c(vecProlifs,c(allTable[[prolifString]][j]))
  }
  
  # check if exactly two division occured!
  if(sum(vecProlifs==2) == 2 & sum(vecProlifs==0) == 0){
    #print(vecProlifs)
    
    # find the distance between the two twos
    firstTwo = -1
    secondTwo = -1
    for(k in 2:length(s)){
      prolifString = paste("prolif",s[k-1],sep="")
      if(allTable[[prolifString]][j] == 2){
        if(firstTwo == -1){
          firstTwo = s[k-1]
        } else {
          secondTwo = s[k-1]
        }
      }
    }
    cellCycleDur = firstTwo - secondTwo
  }
}

# now assemble data frame with cells, their parent labels, proliferation and cell cycle duration

df <- as.data.frame.list(allTable)
colnames(df) <- names(allTable)

# ADD STOMATA DATA
df$stomata <- 0

# load csv files
stomataFolder <- "stomata/"
stomataLast <- NULL
stomataFirst <- NULL

# Wildtype L8 has no stomata
if(currentGenotype == "L1"){
  L1_Last <- read.csv(paste(stomataFolder,"L1_T22_stomata.csv",sep=""),header= T)
  L1_Last <- subset(L1_Last,Parent.Label != 0)
 
  L1_First <- read.csv(paste(stomataFolder,"L1_dataStomata_T5.csv",sep=""),header= T)
  L1_First <- subset(L1_First,Parent.Label != 0)
   
  stomataLast <- L1_Last
  stomataFirst <- L1_First
}
if(currentGenotype == "SPL9r"){
  L1_Last <- read.csv(paste(stomataFolder,"SPL9r_T22_stomata.csv",sep=""),header= T)
  L1_Last <- subset(L1_Last,Parent.Label != 0)
  
  L1_First <- read.csv(paste(stomataFolder,"SPL9r_dataStomata_T5.csv",sep=""),header= T)
  L1_First <- subset(L1_First,Parent.Label != 0)
  
  stomataLast <- L1_Last
  stomataFirst <- L1_First
}

df$stomataLast <- 0
df$stomataFirst <- 0

if(!is_empty(stomataLast) & !is_empty(stomataFirst)){
  for(j in 1:nrow(stomataLast)){
    currentLabel <- stomataLast[j,"Label"]
    for(i in 1:nrow(df)){
      T22Label <- df[i,"label22"]
      splitStringForLabel = str_split(T22Label,"_")
      actualLabel <- splitStringForLabel[[1]][2]
      if(!is.na(actualLabel)){
        if(actualLabel == currentLabel){
          df[i,"stomataLast"] = 1
        }
      }
    }
  }
  
  for(j in 1:nrow(stomataFirst)){
    currentLabel <- stomataFirst[j,"Label"]
    for(i in 1:nrow(df)){
      T5Label <- df[i,"label5"]
      splitStringForLabel = str_split(T5Label,"_")
      actualLabel <- splitStringForLabel[[1]][2]
      if(!is.na(actualLabel)){
        if(actualLabel == currentLabel){
          df[i,"stomataFirst"] = 1
        }
      }

    }
  }
  
}


write.csv(df,paste(SamplePathOut,"/df.csv",sep=""), row.names = FALSE)


# NEW CODE

colDE <- c("CellLabelFirst","CellCycle","Div1_T", "Div1_Label","Div2_T", "Div2_Label", "stomataFirstTP", "stomataLastTP")
divisionEvents <- data.frame(matrix(vector(), 0, length(colDE),
                                   dimnames=list(c(), colDE)),
                            stringsAsFactors=F)

startColumn =paste("label",startT,sep="")
endColumn=paste("label",stopT,sep="")

df_toCheck <- subset(df, totalProlif > 1)  ## if more than 0 and 1 division
df_toCheck <- subset(df_toCheck, totalProlif > 1)  ## if more than 0 and 1 division

uniqueStartLabels = unique(df_toCheck[startColumn])
uniqueStartLabels = uniqueStartLabels[,startColumn]

checkSubTree <- function(df, lastDiv, div1Label, currentLabel, stopT, divisionEventsDF) {
  
  labelCol =paste("label",lastDiv,sep="")
  df_subTree <- subset(df, get(labelCol)==currentLabel)
  #print("number of rows")
  #print(nrow(df_subTree))
  
  # find next division
  counter = lastDiv+1
  checkColumn =paste("prolif",counter,sep="")
  found = FALSE
  
  cellCycle = -1
  
  while(counter < stopT & !found){
    counter = counter + 1
    checkColumn =paste("prolif",counter,sep="")
    if(df_subTree[1, checkColumn] > 1){
      found = TRUE
    }
    
  }
  
  
  #print("looking for next div")
  #print(currentLabel)
  # compute cell cycle
  if(found){
    # add a division
    # split the subTree here, recursive search
    cellCycle = counter - lastDiv
    
    newRow = data.frame(matrix(vector(), 1, length(colDE),
                               dimnames=list(c(), colDE)))
    colN =paste("label",counter,sep="")
    newRow[1,"CellLabelFirst"] = as.character(div1Label)#df[1, colN])
    newRow[1,"CellCycle"] = cellCycle
    newRow[1,"Div1_T"] = lastDiv
    newRow[1,"Div2_T"] = counter
    newRow[1,"Div1_Label"] = div1Label
    
    # continue searching
    lastDiv = counter
    currentColumn <- paste("label",counter,sep="")
    previousColumn <- paste("label",counter-1,sep="")
    # split the subTree here, recursive search
    daughterLabels <- unique(df_subTree[currentColumn])
    daughterLabels = daughterLabels[,currentColumn]
    
    divLabel <- as.character(df_subTree[1,previousColumn])
    
    newRow[1,"Div2_Label"] = divLabel
    newRow[1,"stomataFirstTP"] = df_subTree[1,"stomataFirst"]
    newRow[1,"stomataLastTP"] = df_subTree[1,"stomataLast"]
    
    divisionEventsDF <- rbind(divisionEventsDF,newRow)
    
    #print("next div found")
    #print(div1Label)
    #print(newRow[1,"Div2_Label"])
    #print(daughterLabels)
    #print(lastDiv)
    
    if(counter < stopT){
      
      if(length(daughterLabels) == 1){ # only 1 daughter with labels
        divisionEventsDF <- checkSubTree(df_subTree, lastDiv, divLabel, daughterLabels[1], stopT, divisionEventsDF)
      } else if(length(daughterLabels) == 2){ # 2 daughters with labels
        divisionEventsDF <- checkSubTree(df_subTree, lastDiv, divLabel, daughterLabels[1], stopT, divisionEventsDF)
        divisionEventsDF <- checkSubTree(df_subTree, lastDiv, divLabel, daughterLabels[2], stopT, divisionEventsDF)
      } else { # we ignore all other cases for now
      }
    }
    
  } else { # nothing found
    # abort
  }
  
  return(divisionEventsDF)
}

# for all cells of first tp
for(i in uniqueStartLabels){
  df_subTree <- subset(df_toCheck, get(startColumn)==i)
  # go forward in time until 1st division is found
  counter = startT+1
  checkColumn =paste("prolif",counter,sep="")
  found = FALSE

  while(counter < stopT & !found){
    counter = counter + 1
    checkColumn =paste("prolif",counter,sep="")
    if(df_subTree[1, checkColumn] > 1){
      found = TRUE
    }
    
  }
  if(found){
    # add a division
    lastDiv = counter
    currentColumn <- paste("label",counter,sep="")
    previousColumn <- paste("label",counter-1,sep="")
    # split the subTree here, recursive search
    daughterLabels <- unique(df_subTree[currentColumn])
    daughterLabels = daughterLabels[,currentColumn]
    
    firstDivLabel <- as.character(df_subTree[1,previousColumn])
    
    #print("first div found!")
    #print(daughterLabels)
    #print(lastDiv)
    #print(firstDivLabel)
    
    if(length(daughterLabels) == 1){ # only 1 daughter with labels
      divisionEvents <- checkSubTree(df_subTree, lastDiv, firstDivLabel, daughterLabels[1], stopT, divisionEvents)
    } else if(length(daughterLabels) == 2){ # 2 daughters with labels

      divisionEvents <- checkSubTree(df_subTree, lastDiv, firstDivLabel, daughterLabels[1], stopT, divisionEvents)
      divisionEvents <- checkSubTree(df_subTree, lastDiv, firstDivLabel, daughterLabels[2], stopT, divisionEvents)
    } else { # we ignore all other cases for now
      
    }
  } else { # nothing found
    # abort
  }
  
}

divisionEvents$genotype = currentGenotype
df$genotype = currentGenotype
df$sample = currentSample
divisionEvents$CellCycle = hoursBetweenTPs * divisionEvents$CellCycle







