# plot Ch Oxford leaf1 versus leaf8
SamplePathOut <- "Out_Oxford/"


plotIndividualSamples <- FALSE
includeIncompleteBins <- TRUE # include bins that don't have a value for all of the samples?
excludeBinsFewerSamples <- 2

# colmuns to be selected from csv files, make sure all of them exist for every file
SelectColumns <- c("Sample","Genotype","Parent","DisBaseEuclidean","DisMidEuclidean","CellAreaT1","CellAreaT2","GrowthAreaT1T2","ProliferationT1T2","GrowthAnisotropyT1T2")

# measures for heat maps to be plotted
matrixMeasures <- c("PercentageCellsT1","PercentageCellsT2","PercentageAreaT1","PercentageAreaT2",
                    "GrowthAnisotropyT1T2","ProliferationT1T2","CellAreaT1","CellAreaT2")

useManualLimits = TRUE
# limits for individual plots
limitsMeasuresSingleSample=list(c(0,1000),c(0,20000),c(1,200),c(1,200),c(0,0.08),c(0,0.08),c(0,0.08),c(0,0.08),c(1,6),c(1,6))

# limits for alignment plots
matrixMeasuresToPlot <- c("PercentageCellsT2","PercentageAreaT2","sampleCounter")
matrixMeasuresLimitforsize <- list(c(0.00,0.15),c(0.00,0.10),c(1,4))
matrixMeasuresLimits <- list(c(0.00,0.13),c(0.00,0.09),c(1,4))#,c(0.00,0.09),c(0.00,0.09),c(1,6),c(1,6),c(1,6),c(1,6),c(1,6))
matrixMeasuresMidpoint <- list(0.03,0.02,2)#,0.0333,1.5,1.5,1.5,1.5,1.5)
matrixColorScheme <- list("French","Jet","Jet","Jet","Jet","Jet","Jet","Jet","Jet")
matrixTitle <- list("Cellular Contr.","Areal Contr.","Samples")#,"Areal Contr. T1","Areal Contr. T2","GrowthAniso","Prolif","AreaT1","AreaT2","Samples")


# number of bins for both, PD & ML
mSize = 7

AllSampleNames<- c("OxL8_07_T1T4","OxL8_09_T1T4","OxL8_15_T1T4","OxL8_17_T1T4",
                   "OxL1_03_T0T3","OxL1_04_T1T4","OxL1_06_T1T4","OxL1_07_T3T6")
GenotypeVec <- c("OxL8","OxL8","OxL8","OxL8",
                 "OxL1","OxL1","OxL1","OxL1")

source("Plot2D_v6s.R")

