# R analysis for Li et al

This repository contains custom data analysis scripts developed for the paper Li et al. 2023 "Cell cycle-driven growth reprogramming encodes plant age into leaf morphogenesis"

Combine_Lineages
contains the R script ("combinelineages.R") used to combine cell lineage ("parent label") CSV files. The folder "example" contains the script an some test files.
To run the script run the following command from command line (or from RStudio) "Rscript combinelineages.r T0T1.csv T1T2.csv T2T3.csv T3T4.csv". The script will creae
