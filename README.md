# R analysis for Li et al

This repository contains custom data analysis scripts developed for the paper Li et al. 2023 "Cell cycle-driven growth reprogramming encodes plant age into leaf morphogenesis"

The repository is organized as follows:

Alignment_2D:
contains raw files (CSV files exported from MorphoGraphX), R-scripts and plots.
The scripts generate 2D alignment plots from raw cellular data. The data is arranged and binned by proximal-distal and medial-lateral position

Cell_cycle
contains CSV cell lineage tracking files from MorphoGraphX, R-scripts and plots.
The scripts load the cell ineages and compute the duration of cell cycles as the time duration between two division events

Combine_Lineages
contains the R script ("combinelineages.R") used to combine cell lineage ("parent label") CSV files. The folder "example" contains the script an some test files.
To run the script run the following command from command line (or from RStudio) "Rscript combinelineages.r T0T1.csv T1T2.csv T2T3.csv T3T4.csv". The script will creae
